<?php

namespace app\controllers;

use Yii;
use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emple models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Emple();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionConsulta1() {
        /* Utilizar el ActiveQuery */
        $r = Emple::find();

        /* Utilizar un dataprovider desde activeQuery */
        $d = new ActiveDataProvider([
            "query" => $r
        ]);

        $consulta = $r->all();

        /* Crear una consulta con QueryBuilder */
        $listado = (new Query())->select("*")->from("emple")->all();

        /*
          Crear una consulta con el objeto conexión
         */
        $consulta = Yii::$app->db->createCommand("select * from emple");

        return $this->render('consulta1', [
                    'datos' => $consulta,
                    'dataProvider' => $d,
                    'listado' => $listado,
                    'consulta' => $consulta
        ]);
    }

    public function actionConsulta3() {
        $consulta = Emple::find()
                ->select("apellido,oficio");

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta3', [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta6() {
        $consulta = Emple::find()->count();

        return $this->render('consulta6', [
                    'titulo' => 'Consulta 6',
                    'texto' => "Indicar el número de empleados que hay",
                    "datos" => $consulta
        ]);
    }

    public function actionConsulta7() {
        $consulta = Emple::find()
                ->orderBy('apellido');

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta7', [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta8() {
        $consulta = Emple::find()
                ->orderBy(['apellido' => SORT_DESC]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta8', [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta10() {
        $consulta = Emple::find()
                ->orderBy(['dept_no' => SORT_DESC]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta10', [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta11() {
        $consulta = Emple::find()
                ->orderBy(['dept_no' => SORT_DESC,
            'oficio' => SORT_ASC]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta11', [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta12() {
        $consulta = Emple::find()
                ->orderBy(['dept_no' => SORT_DESC,
            'apellido' => SORT_ASC]);


        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta12', [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta13() {
        $consulta = Emple::find()
                ->select('emp_no')
                ->where('salario>2000');

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta13', [
                    'titulo' => 'Consulta 13',
                    'texto' => "Mostrar los códigos de los empleados cuyo salario sea mayor 2000",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta14() {
        $consulta = Emple::find()
                ->select('emp_no,apellido')
                ->where('salario<2000');

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta14', [
                    'titulo' => 'Consulta 14',
                    'texto' => "Mostrar los códigos y los apellidos de los empleados cuyo salario sea menor 2000",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta15() {
        $consulta = Emple::find()
                ->where(['between', 'salario', '1500', '2500']);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta15', [
                    'titulo' => 'Consulta 15',
                    'texto' => "Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta16() {
        $consulta = Emple::find()
                ->where(['oficio' => 'ANALISTA']);


        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta16', [
                    'titulo' => 'Consulta 16',
                    'texto' => "Mostrar los datos de los empleados cuyo oficio sea 'ANALISTA'",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta17() {
        $consulta = Emple::find()
                ->asArray()
                ->where(['and',
            ['oficio' => 'ANALISTA'],
            ['>', 'salario', '2000']
        ]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta17', [
                    'titulo' => 'Consulta 17',
                    'texto' => "Mostrar los datos de los empleados cuyo oficio sea 'ANALISTA' y ganen mas de 2000€",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta18() {
        $consulta = Emple::find()
                ->select('apellido,oficio')
                ->where(['dept_no' => 20]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta18', [
                    'titulo' => 'Consulta 18',
                    'texto' => "Mostrar el apellido y oficio de los empleados del departamento número 20",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta19() {
        $consulta = Emple::find()
                ->where(['oficio' => 'VENDEDOR'])
                ->count();


        return $this->render('consulta19', [
                    'titulo' => 'Consulta 19',
                    'texto' => "Contar el número de empleados cuyo oficio sea 'VENDEDOR'",
                    'datos' => $consulta
        ]);
    }

    public function actionConsulta20() {
        $consulta = Emple::find()
                ->where(['or', 'left(apellido,1)="m"', 'left(apellido,1)="n"'])
                ->orderBy(['apellido' => SORT_ASC]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta20', [
                    'titulo' => 'Consulta 20',
                    'texto' => "Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendete",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta21() {
        $consulta = Emple::find()
                ->where(['oficio' => 'VENDEDOR'])
                ->orderBy(['apellido' => SORT_ASC]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta21', [
                    'titulo' => 'Consulta 21',
                    'texto' => "Seleccionar los empleados cuyo oficio sea 'VENDEDOR'. Mostrar los datos ordenados por apellido de forma ascendente",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta22() {
        $c1 = Emple::find()->max('salario');

        $consulta = Emple::find()
                ->select('apellido')
                ->where(['salario' => $c1]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta22', [
                    'titulo' => 'Consulta 22',
                    'texto' => "Mostrar los apellidos del empleado que mas gana",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta23() {
        $consulta = Emple::find()
                ->where(['dept_no'=>10,
                         'oficio'=>'ANALISTA'])
                ->orderBy(['apellido' => SORT_ASC,
                           'oficio' => SORT_ASC]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta23', [
                    'titulo' => 'Consulta 23',
                    'texto' => "Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea 'ANALISTA'. Ordenar el resultado por apellido y oficio de forma ascendente",
                    'datos' => $dp
        ]);
    }
    
    
    public function actionConsulta24() {
        $consulta = Emple::find()
                ->distinct()
                ->select('MONTH(fecha_alt) as fecha_alt');

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta24', [
                    'titulo' => 'Consulta 24',
                    'texto' => "Realizar un listado de los distintos meses en que los empleados se han dado de alta",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta25() {
        $consulta = Emple::find()
                ->distinct()
                ->select('YEAR(fecha_alt) as fecha_alt');

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta25', [
                    'titulo' => 'Consulta 25',
                    'texto' => "Realizar un listado de los distintos años en que los empleados se han dado de alta",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta26() {
        $consulta = Emple::find()
                ->distinct()
                ->select('DAY(fecha_alt) as fecha_alt');

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta26', [
                    'titulo' => 'Consulta 26',
                    'texto' => "Realizar un listado de los distintos dias del mes en que los empleados se han dado de alta",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta27() {
        $consulta = Emple::find()                
                ->select('apellido')             
                ->where(['or',['>','salario','2000'],
                         ['dept_no'=>20]]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta27', [
                    'titulo' => 'Consulta 27',
                    'texto' => "Mostrar los apellidos de los empleados que tengan un salario mayor de 2000 o que pertenezcan al departamento 20",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta28() {
        $consulta = Emple::find();

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta28', [
                    'titulo' => 'Consulta 28',
                    'texto' => "Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta29() {
        $consulta = Emple::find();

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta29', [
                    'titulo' => 'Consulta 29',
                    'texto' => "Realizar un listado donde nos coloque el apellido del empleado,"
                              ." el oficio del empleado y el nombre del departamento al que pertenece."
                              ." Ordenar los resultados por apellido de forma descendente",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta30() {
        $consulta = Emple::find()
                ->select('dept_no,count(*) as numero')
                ->groupBy('dept_no')
                ;    
        
        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta30', [
                    'titulo' => 'Consulta 30',
                    'texto' => "Listar el número de empleados por departamento. La salida del comando debe ser como la que vemos a continuación",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta31() {
        $consulta = Emple::find()
                ->select('dept_no,count(*) as numero')
                ->groupBy('dept_no');    
        
        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta31', [
                    'titulo' => 'Consulta 31',
                    'texto' => "Realizar el mismo comando anterior pero obteniendo una salida como la que vemos",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta32() {
        $consulta = Emple::find()
                ->select('apellido')                
                ->orderBy(['oficio' => SORT_ASC,
                           'apellido' => SORT_ASC]);    
        
        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta32', [
                    'titulo' => 'Consulta 32',
                    'texto' => "Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta33() {
        $consulta = Emple::find()
                ->select('apellido')                
                ->where('left(apellido,1)="a"');    
        
        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta33', [
                    'titulo' => 'Consulta 33',
                    'texto' => "Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por 'A'."
                               ." Listar el apellido de los empleados",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta34() {
        $consulta = Emple::find()
                ->select('apellido')                
                ->where(['or','left(apellido,1)="a"','left(apellido,1)="m"']);    
        
        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta34', [
                    'titulo' => 'Consulta 34',
                    'texto' => "Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por 'A' o por 'M'."
                               ." Listar el apellido de los empleados",
                    'datos' => $dp
        ]);
    }
    
    public function actionConsulta35() {
        $consulta = Emple::find()                              
                ->where(['not','right(apellido,1)="z"']);    
        
        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta35', [
                    'titulo' => 'Consulta 35',
                    'texto' => "Seleccionar de la tabla EMPLE los empleados cuyo apellido no empiece por 'Z'."
                               ." Listar todos los campos de la tabla empleados",
                    'datos' => $dp
        ]);
    }

}


