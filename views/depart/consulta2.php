<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depart-index">

    <h1><?= Html::encode($this->title) ?></h1>       
    
    <h2>Listando el Active Record</h2>
    <?php
        var_dump($activeRecord);
    ?>
    
    <h2> Listar con GridView </h2>
    
    <?= GridView::widget([
        'dataProvider' => $dp,
        'columns' => [          
            'dept_no',
            'dnombre',
            'loc',
        ],
    ]); ?>
    
</div>
