<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depart-index">

    <h1><?= Html::encode($this->title) ?></h1>               
    
    <h2> Listar con GridView con paginación </h2>
    
    <?= GridView::widget([
        'dataProvider' => $datos,
        'columns' => [ 
            'loc',
            'dept_no',
            'dnombre'
        ],
    ]); ?>
    
    
</div>
