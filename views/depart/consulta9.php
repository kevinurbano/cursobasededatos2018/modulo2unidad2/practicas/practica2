<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depart-index">

    <h1><?= $titulo ?></h1>
    
    <h2><?= $texto ?></h2>
    
    <h3>Total Departamentos: <?= $datos ?></h3> 
        
</div>
