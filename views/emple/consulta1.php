<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <h2>Mostrando con DataProvider</h2>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [            
            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            'salario',
            'comision',
            'dept_no',    
            'deptNo.dnombre',
            'deptNo.loc'
        ],
    ]); ?>
    
    
    <div>
        <h2>Utilizando Active Query</h2>
        <?php
            var_dump($datos);            
            
        ?>
    </div>
    
    <div>
        <h2>Utilizando el QueryBuilder</h2>
        <?php
            var_dump($listado);           
            
        ?>
    </div>
    
    <div>
        <h2>Mostrando con DAO</h2>
        <?php
            var_dump($consulta);                       
        ?>
    </div>
    
    
    
            
</div>