<?php
//precargar las clase
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Consultas';
?>
<div class="site-index">
    
    
    
  
    <div class="jumbotron">
        <h1>Practica 2</h1>
        
        <p class="lead">Consultas</p>
        
    </div>    

    <div class="body-content">

        <div class="row">
            
            <div class="col-md-2">
                <h2>Consulta 1</h2>

                <p>Listar a todos los empleados</p>

                <p><?= Html::a("Ejecutar",["emple/consulta1"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 2</h2>

                <p>Mostrar todos los departamentos</p>

                <p><?= Html::a("Ejecutar",["depart/consulta2"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 3</h2>

                <p>Mostrar el apellido y oficio </p>

                <p><?= Html::a("Ejecutar",["emple/consulta3"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 4</h2>

                <p>Mostrar localización y numero de departamento</p>

                <p><?= Html::a("Ejecutar",["depart/consulta4"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 5</h2>

                <p>Mostrar el número, nombre y la localización de cada departamento</p>

                <p><?= Html::a("Ejecutar",["depart/consulta5"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 6</h2>

                <p>Indicar el númerode empleados que hay.</p>

                <p><?= Html::a("Ejecutar",["emple/consulta6"],["class"=>"btn btn-default"]) ?></p>
            </div>
                        
                                    
        </div>
        
        <div class="row">      
            
            <div class="col-lg-2">
                <h2>Consulta 7</h2>

                <p>Datos de los empleados ordenados por apellido de forma ascendente</p>

                <p><?= Html::a("Ejecutar",["emple/consulta7"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 8</h2>

                <p>Datos de los empleados ordenados por apellido de forma descendente</p>

                <p><?= Html::a("Ejecutar",["emple/consulta8"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 9</h2>

                <p>Indicar el número de departamentos que hay</p>

                <p><?= Html::a("Ejecutar",["depart/consulta9"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 10</h2>

                <p>Datos de los empleados ordenados por el número de departamento de forma descendente</p>

                <p><?= Html::a("Ejecutar",["emple/consulta10"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 11</h2>

                <p>Datos de los empleados ordenados por el número de departamento de forma descendente y`por oficio ascendente</p>

                <p><?= Html::a("Ejecutar",["emple/consulta11"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 12</h2>

                <p>Datos de los empleados ordenados por el número de departamento de forma descendente y`por apellido ascendente</p>

                <p><?= Html::a("Ejecutar",["emple/consulta12"],["class"=>"btn btn-default"]) ?></p>
            </div>   
            
            
        </div>
        
        <div class="row">          
            
            <div class="col-lg-2">
                <h2>Consulta 13</h2>

                <p>Mostrar los códigos de los empleados cuyo salario sea mayor 2000</p>

                <p><?= Html::a("Ejecutar",["emple/consulta13"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 14</h2>

                <p>Mostrar los códigos de los empleados cuyo salario sea mayor 2000</p>

                <p><?= Html::a("Ejecutar",["emple/consulta14"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 15</h2>

                <p>Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500</p>

                <p><?= Html::a("Ejecutar",["emple/consulta15"],["class"=>"btn btn-default"]) ?></p>
            </div>
                       
            <div class="col-lg-2">
                <h2>Consulta 16</h2>

                <p>Mostrar los datos de los empleados cuyo oficio sea 'ANALISTA'</p>

                <p><?= Html::a("Ejecutar",["emple/consulta16"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 17</h2>

                <p>Mostrar los datos de los empleados cuyo oficio sea 'ANALISTA' y ganen mas de 2000€</p>

                <p><?= Html::a("Ejecutar",["emple/consulta17"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-lg-2">
                <h2>Consulta 18</h2>

                <p>Mostrar el apellido y oficio de los empleados del departamento número 20</p>

                <p><?= Html::a("Ejecutar",["emple/consulta18"],["class"=>"btn btn-default"]) ?></p>
            </div>
                        
            
        </div>
        
        <div class="row">
            
            <div class="col-md-2">
                <h2>Consulta 19</h2>

                <p>Contar el número de empleados cuyo oficio sea 'VENDEDOR'</p>

                <p><?= Html::a("Ejecutar",["emple/consulta19"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 20</h2>

                <p>Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendete</p>

                <p><?= Html::a("Ejecutar",["emple/consulta20"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 21</h2>

                <p>Seleccionar los empleados cuyo oficio sea 'VENDEDOR'. Mostrar los datos ordenados por apellido de forma ascendente</p>

                <p><?= Html::a("Ejecutar",["emple/consulta21"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 22</h2>

                <p>Mostrar los apellidos del empleado que mas gana</p>

                <p><?= Html::a("Ejecutar",["emple/consulta22"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 23</h2>

                <p>Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea 'ANALISTA'. Ordenar el resultado por apellido y oficio de forma ascendente</p>

                <p><?= Html::a("Ejecutar",["emple/consulta23"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 24</h2>

                <p>Realizar un listado de los distintos meses en que los empleados se han dado de alta</p>

                <p><?= Html::a("Ejecutar",["emple/consulta24"],["class"=>"btn btn-default"]) ?></p>
            </div>            
                        
                                    
        </div>
        
        <div class="row">
            
            <div class="col-md-2">
                <h2>Consulta 25</h2>

                <p>Realizar un listado de los distintos años en que los empleados se han dado de alta</p>

                <p><?= Html::a("Ejecutar",["emple/consulta25"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 26</h2>

                <p>Realizar un listado de los distintos dias del mes en que los empleados se han dado de alta</p>

                <p><?= Html::a("Ejecutar",["emple/consulta26"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 27</h2>

                <p>Mostrar los apellidos de los empleados que tengan un salario mayor de 2000 o que pertenezcan al departamento 20</p>

                <p><?= Html::a("Ejecutar",["emple/consulta27"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 28</h2>

                <p>Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece</p>

                <p><?= Html::a("Ejecutar",["emple/consulta28"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 29</h2>

                <p>Realizar un listado donde nos coloque el apellido del empleado 
                   el oficio del empleado y el nombre del departamento al que pertenece. 
                   Ordenar los resultados por apellido de forma descendente</p>

                <p><?= Html::a("Ejecutar",["emple/consulta29"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 30</h2>

                <p>Listar el número de empleados por departamento. La salida del comando debe ser como la que vemos a continuación</p>

                <p><?= Html::a("Ejecutar",["emple/consulta30"],["class"=>"btn btn-default"]) ?></p>
            </div>                                                                                           
                     
            
        </div>
        
        
        <div class="row">            
           
            <div class="col-md-2">
                <h2>Consulta 31</h2>

                <p>Realizar el mismo comando anterior pero obteniendo una salida como la que vemos</p>

                <p><?= Html::a("Ejecutar",["emple/consulta31"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 32</h2>

                <p>Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre</p>

                <p><?= Html::a("Ejecutar",["emple/consulta32"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 33</h2>

                <p>Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por 'A'. Listar el apellido de los empleados</p>

                <p><?= Html::a("Ejecutar",["emple/consulta33"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 34</h2>

                <p>Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por 'A' o por 'M'. Listar el apellido de los empleados</p>

                <p><?= Html::a("Ejecutar",["emple/consulta34"],["class"=>"btn btn-default"]) ?></p>
            </div>
            
            <div class="col-md-2">
                <h2>Consulta 35</h2>

                <p>Seleccionar de la tabla EMPLE los empleados cuyo apellido no empiece por 'Z'. Listar todos los campos de la tabla empleados</p>

                <p><?= Html::a("Ejecutar",["emple/consulta35"],["class"=>"btn btn-default"]) ?></p>
            </div>
                        
                                    
        </div>
            
        


    </div>
    
</div>
